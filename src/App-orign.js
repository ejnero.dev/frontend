import React from 'react';
import Login from './components/Login';
import Datagraph from './components/Datagraph';

function App() {
  return (
    <div className="bg-gradient-to-t from-blue-200 to-white min-h-screen">
      <h1 className="text-center text-2xl font-bold text-indigo-600 sm:text-3xl">LOGIN</h1>
      <Login />   
    </div>
  );
}

export default App;
