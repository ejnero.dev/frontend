import React, { useState } from 'react';
import Login from './components/Login';
import Datagraph from './components/Datagraph';

function App() {
  const [isLoggedIn, setIsLoggedIn] = useState(false); // State to track login status

  const handleLogin = (loginStatus) => {
    setIsLoggedIn(loginStatus); // Update login status on login/logout
  };

  return (
    <div className="bg-gradient-to-t from-blue-200 to-white min-h-screen">
      <h1 className="text-center text-2xl font-bold text-indigo-600 sm:text-3xl">LOGIN</h1>
      <Login onLogin={handleLogin} /> {/* Pass handleLogin function to Login component */}
      {isLoggedIn && <Datagraph />} {/* Render Datagraph only if isLoggedIn is true */}
    </div>
  );
}

export default App;
