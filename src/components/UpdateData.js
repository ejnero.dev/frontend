import React, { useState, useEffect } from 'react';
import axios from 'axios';

const UpdateData = ({ userData }) => {
  const [fullname, setFullname] = useState(userData?.fullname || '');
  const [email, setEmail] = useState(userData?.email || '');
  const [phone, setPhone] = useState(userData?.phone || '');
  const [age, setAge] = useState(userData?.age || '');
  const [isLoggedIn, setIsLoggedIn] = useState(false); // Nuevo estado para controlar si el usuario está conectado

  useEffect(() => {
    axios.get(`http://localhost:5000/user/${userData.name}`)
    .then((response) => {
      const data = response.data;
      setFullname(data.fullname);
      setAge(data.age);
      setEmail(data.email);
      setPhone(data.phone);
      setIsLoggedIn(true); // Establece isLoggedIn a true después de obtener los datos del usuario
    });
  }, []);

  const handleSubmit = (event) => {
    event.preventDefault();

    axios.put('http://localhost:5000/update-data', {
      "fullname" : fullname,
      "email" : email,
      "phone" : phone,
      "age" : Number(age),
      "name" : userData?.name || '',
    })
    .then(response => {
      if (response.status === 200 || response.status === 201) {
        console.log("Datos actualizados correctamente");
      } else {
        console.log("Algo falló al actualizar", response.status);
      }
    })
    .catch(error => {
      console.error('Error al actualizar:', error);
    });
  };

  return (
<form onSubmit={handleSubmit} className="flex flex-col space-y-4 bg-gradient-to-b from-blue-200 to-white p-6 rounded-lg shadow-md">
      <p className="text-center text-lg font-medium">
        {isLoggedIn ? `Bienvenido ${userData.name}, ¿desea cambiar sus datos?` : 'Por favor, introduce tus credenciales para iniciar sesión.'}
      </p>
      <input
        type="text"
        value={fullname}
        onChange={e => setFullname(e.target.value)}
        className="w-full px-3 py-2 rounded-md border border-gray-300 focus:outline-none focus:ring-2 focus:ring-blue-500 focus:border-blue-500"
        placeholder="Nombre completo"
      />
      <input
        type="text"
        value={email}
        onChange={e => setEmail(e.target.value)}
        className="w-full px-3 py-2 rounded-md border border-gray-300 focus:outline-none focus:ring-2 focus:ring-blue-500 focus:border-blue-500"
        placeholder="Correo"
      />
      <input
        type="text"
        value={phone}
        onChange={e => setPhone(e.target.value)}
        className="w-full px-3 py-2 rounded-md border border-gray-300 focus:outline-none focus:ring-2 focus:ring-blue-500 focus:border-blue-500"
        placeholder="Telefono"
      />
      <input
        type="number"
        value={age}
        onChange={e => setAge(e.target.value)}
        className="w-full px-3 py-2 rounded-md border border-gray-300 focus:outline-none focus:ring-2 focus:ring-blue-500 focus:border-blue-500"
        placeholder="Edad"
      />
      <button
        type="submit"
        className="block w-full rounded-lg bg-blue-500 hover:bg-blue-700 text-white font-bold px-5 py-3"
      >
        Actualizar
      </button>
    </form>

  );
};

export default UpdateData;
