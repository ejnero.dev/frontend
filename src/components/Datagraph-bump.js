import React, { useState, useEffect } from 'react';
import { ResponsiveBump } from '@nivo/bump';

const MyElectricalBump = ({ data }) => (
  <ResponsiveBump
    data={data}
    colors={{ scheme: 'paired' }}
    lineWidth={2}
    activeLineWidth={4}
    inactiveLineWidth={2}
    inactiveOpacity={0.2}
    pointSize={8}
    activePointSize={12}
    inactivePointSize={0}
    pointColor={{ theme: 'background' }}
    pointBorderWidth={2}
    activePointBorderWidth={2}
    pointBorderColor={{ from: 'serie.color' }}
    axisTop={{
      tickSize: 5,
      tickPadding: 5,
      tickRotation: 0,
      legend: 'Time',
      legendPosition: 'middle',
      legendOffset: -36,
    }}
    axisBottom={{
      tickSize: 5,
      tickPadding: 5,
      tickRotation: 0,
      legend: '',
    }}
    axisLeft={{
      tickSize: 5,
      tickPadding: 5,
      tickRotation: 0,
      legend: 'Tension (V)',
      legendPosition: 'middle',
      legendOffset: -40,
    }}
    margin={{ top: 40, right: 100, bottom: 40, left: 60 }}
    axisRight={null}
  />
);



const Datagraph = () => {
    const [data, setData] = useState([]);
    const [error, setError] = useState(null);
    const [loading, setLoading] = useState(true);
  
    useEffect(() => {
        fetch('http://localhost:5000/data')
          .then((response) => {
            if (!response.ok) {
              throw new Error('Error al obtener los datos');
            }
            return response.json();
          })
          .then((responseData) => {
            // Transforma los datos al formato esperado por Nivo
            const transformedData = responseData.reduce((acc, curr) => {
              const { tiempo, tension_salida } = curr;
              let item = acc.find((i) => i.id === 'tension_salida');
              if (!item) {
                item = { id: 'tension_salida', data: [] };
                acc.push(item);
              }
              item.data.push({ x: tiempo, y: tension_salida });
              return acc;
            }, []);
            setData(transformedData);
            setLoading(false);
          })
          .catch((error) => {
            console.error('Error fetching or processing data:', error);
            setError('Error retrieving data. Please try again later.');
            setLoading(false);
          });
      }, []);
    
      if (loading) {
        return <div>Loading data...</div>;
      }
    
      if (error) {
        return <div>Error: {error}</div>;
      }
    
      return (
        <div>
          {data.length > 0 ? (
            <MyElectricalBump data={data} />
          ) : (
            <p>No data available</p>
          )}
        </div>
      );
    };
    
    export default Datagraph;