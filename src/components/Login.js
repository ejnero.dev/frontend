import React, { useState, useEffect } from 'react';
import axios from 'axios';
import UpdateData from './UpdateData'; // Assuming UpdateData is a component
import Datagraph from './Datagraph';

const Login = () => {
  const [user, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [loggedIn, setLoggedIn] = useState(false);
  const [userData, setUserData] = useState(null); // State to store user data (optional)

  // Improved login handling using async/await
  const handleLogin = async (event) => {
    event.preventDefault();
    if (user && password) {
      try {
        const response = await axios.post('http://localhost:5000/validate-user', {
          name: user,
          password,
        });

        if (response.status === 200) {
          setLoggedIn(true);
          console.log('Logged in');
          // Optionally store user data if needed
          setUserData(response.data); // Assuming response.data contains user info
        } else {
          console.log('Invalid user or password');
          setLoggedIn(false);
        }
      } catch (error) {
        console.error('Error:', error);
      }
    } else {
      console.log('Please enter user and password');
    }
  };

  // Check login status on component mount or update
  useEffect(() => {
    const storedLoggedIn = localStorage.getItem('loggedIn'); // Check for stored login state
    if (storedLoggedIn) {
      setLoggedIn(JSON.parse(storedLoggedIn)); // Parse stored value (optional)
    }
  }, []); // Run only once on initial render

  return (
<div className="mx-auto max-w-screen-xl px-4 py-16 sm:px-6 lg:px-8">
  <div className="mx-auto max-w-full">
    
    <p className="mx-auto mt-4 max-w-md text-center text-gray-500">
      BDLogin acceso y modificación
    </p>
    {!loggedIn && (
      <form onSubmit={handleLogin} className="mb-0 mt-6 space-y-4 rounded-lg p-4 shadow-lg sm:p-6 lg:p-8">
        <p className="text-center text-lg font-medium">Inicia sesión en tu cuenta</p>
        <div>
          <label htmlFor="username" className="sr-only">Usuario</label>
          <div className="relative">
            <input
              type="text"
              value={user}
              onChange={(e) => setUsername(e.target.value)}
              id="username"
              className="w-full rounded-lg border-gray-200 p-4 pe-12 text-sm shadow-sm"
              placeholder="Introduce tu usuario"
            />
          </div>
        </div>
        <div>
          <label htmlFor="password" className="sr-only">Contraseña</label>
          <div className="relative">
            <input
              type="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              id="password"
              className="w-full rounded-lg border-gray-200 p-4 pe-12 text-sm shadow-sm"
              placeholder="Introduce tu contraseña"
            />
          </div>
        </div>
        <button
          type="submit"
          className="block w-full rounded-lg bg-indigo-600 px-5 py-3 text-sm font-medium text-white"
        >
          Iniciar sesión
        </button>
        <p className="text-center text-sm text-gray-500">
          ¿No tienes una cuenta?
          <a className="underline" href="#">Regístrate</a>
        </p>
      </form>
    )}
    {/* {loggedIn && userData && (
      <UpdateData userData={userData} />
    )}
    {loggedIn && !userData && (
      <p>Cargando datos del usuario...</p>
    )} */}
    {loggedIn && <Datagraph />}
  </div>
</div>


  );
};

export default Login;
