import React, { useState, useEffect } from 'react';
import { ResponsiveScatterPlot } from '@nivo/scatterplot';

const MyElectricalScatterplot = ({ data }) => (
  <ResponsiveScatterPlot
    data={data}
    margin={{ top: 50, right: 150, bottom: 70, left: 80 }} // Increased width and height margins
    xScale={{ type: 'point' }}
    yScale={{ type: 'linear', min: 'auto', max: 'auto' }}
    axisTop={null}
    axisRight={null}
    axisBottom={{
      orient: 'bottom',
      tickSize: 5,
      tickPadding: 5,
      tickRotation: 0,
      legend: 'Time',
      legendOffset: 36,
      legendPosition: 'middle',
    }}
    axisLeft={{
      orient: 'left',
      tickSize: 5,
      tickPadding: 5,
      tickRotation: 0,
      legend: 'Tension (V)',
      legendOffset: -40,
      legendPosition: 'middle',
    }}
    colors={{ scheme: 'category10' }} // Vibrant colors
    pointSize={10}
    pointColor={{ theme: 'background' }}
    pointBorderWidth={2}
    pointBorderColor={{ from: 'serieColor' }}
    useMesh={true}
    enableSlices="x"
    sliceTooltip={({ slice }) => (
      <div
        style={{
          background: 'white',
          padding: '9px 12px',
          border: '1px solid #ccc',
        }}
      >
        {slice.points.map((point) => (
          <div key={point.id} style={{ color: point.serieColor, padding: '3px 0' }}>
            <strong>{point.serieId}</strong> [{point.data.xFormatted}]: {point.data.yFormatted}
          </div>
        ))}
      </div>
    )}
  />
);

const Datagraph = () => {
  const [data, setData] = useState([]);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    fetch('http://localhost:5000/data')
      .then((response) => {
        if (!response.ok) {
          throw new Error('Error al obtener los datos');
        }
        return response.json();
      })
      .then((responseData) => {
        const transformedData = responseData.reduce((acc, curr) => {
          const { tiempo, tension_salida } = curr;
          let item = acc.find((i) => i.id === 'tension_salida');
          if (!item) {
            item = { id: 'tension_salida', data: [] };
            acc.push(item);
          }
          item.data.push({ x: tiempo, y: tension_salida });
          return acc;
        }, []);
        setData(transformedData);
        setLoading(false);
      })
      .catch((error) => {
        console.error('Error fetching or processing data:', error);
        setError('Error retrieving data. Please try again later.');
        setLoading(false);
      });
  }, []);

  if (loading) {
    return <div>Loading data...</div>;
  }

  if (error) {
    return <div>Error: {error}</div>;
  }

  return (
    <div style={{ height: '600px', width: '100%' }}> {/* Increased height for the chart and set width to 100% */}
      {data.length > 0 ? (
        <MyElectricalScatterplot data={data} />
      ) : (
        <p>No data available</p>
      )}
    </div>
  );
};

export default Datagraph;
