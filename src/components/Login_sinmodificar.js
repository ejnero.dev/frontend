import React, { useState } from 'react';
import axios from 'axios';
//import {UpdateData} from '.components/UpdateData';


const Login = () => {
 const [user, setUsername] = useState('');
 const [password, setPassword] = useState('');
 const [loggedIn, setLoggedIn] = useState(false);


 const handleLogin = (event) => {
   event.preventDefault(); // Prevent default form submission behavior
   if (user && password) {
     console.log('checking password', user, password);
     axios.post('http://localhost:5000/validate-user', { "name": user, "password": password })
       .then(response => {
         console.log('responseAQUIIII', response);
         console.log('User checked');
          console.log('user checked', response.status);
         if (response.status === 200) {
           setLoggedIn(true);
           console.log('Logged in');
         } else {
            console.log('Invalid user or password')
           setLoggedIn(false);
         }
       })
       .catch(error => console.error('Error:', error));
   } else {
     console.log('Please enter user and password');
   }
 };


 return (
   <div>
     {!loggedIn &&
     <form onSubmit={handleLogin}>
       <div>
         <label>Username:</label>
         <input type="text" value={user} onChange={(e) => setUsername(e.target.value)} />
       </div>
       <div>
         <label>Password:</label>
         <input type="password" value={password} onChange={(e) => setPassword(e.target.value)} />
       </div>
       <button type="submit">Login</button>
     </form>
   }
   {loggedIn ? <h2>Logged in</h2> : <h2>Not logged in</h2>}
    
   </div>
 );
};


export default Login;